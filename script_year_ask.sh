#!/bin/bash
read -p "Enter package name: " name
echo "Package name is $name"
read -p "Enter path for package source files: " dir_path
read -p "Enter start year: " year_s
read -p "Enter end year: " year_e

mkdir $dir_path/$name
cd $dir_path/$name

for year_dir in `seq $year_s $year_e`
do
    mkdir $year_dir
    cd $year_dir
    month_list="03 07"
    for month_dir in $month_list
        do
            mkdir $month_dir 
            cd $month_dir
            day_list="15 28"
            for day_of_month in $day_list
                do
                day_mom=`curl -q "https://snapshot.debian.org/archive/debian/?year=$year_dir&month=$month_dir" | grep $year_dir$month_dir$day_of_month | awk -F'"' '{print $2}' | awk -F '/' '{print $1}'| tail -n 1`
                echo "deb [trusted=yes] http://snapshot.debian.org/archive/debian/$day_mom/ stable main" > sources_$day_mom.list
                echo "deb-src [trusted=yes] http://snapshot.debian.org/archive/debian/$day_mom/ stable main" >> sources_$day_mom.list
                echo "deb [trusted=yes] http://snapshot.debian.org/archive/debian/$day_mom/ stable/updates main" >> sources_$day_mom.list
                echo "deb-src [trusted=yes] http://snapshot.debian.org/archive/debian/$day_mom/ stable/updates main" >> sources_$day_mom.list  
                cp sources_$day_mom.list /etc/apt/sources.list -f                                                                              
                apt-get update                                                                                                                 
                apt-get source --download-only --allow-unauthenticated $name
                sleep 5
            done
            cd $dir_path/$name/$year_dir
    done
    cd $dir_path/$name
done
cd $dir_path/$name
chmod 777 * -R
