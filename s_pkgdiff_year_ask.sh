#!/bin/bash
echo "This script MUST BE runned after s_sloccount.sh script!"
read -p "Enter package name: " name
echo "Package name is $name"
read -p "Enter path for package source files (ex: /home/jon/$name): " dir_path
read -p "Enter start year: " year_s
read -p "Enter end year: " year_e

pc_date=`date '+%Y%m%d%H%M'`
cd $dir_path
touch -f "$dir_path/pkgdiff_$pc_date.files"
for year_dir in `seq $year_s $year_e`
do
	cd $dir_path/$year_dir/03
	echo `pwd`	
		archive_f=`ls -S | grep ".tar." | grep ".orig" | grep -v ".diff" | head -1`
		path_archive_f=`readlink -f $archive_f`
		source_dir=`ls --file-type -1 | grep "/" | awk -F '/' '{print $1}'`
		cd $dir_path/$year_dir/03/$source_dir
		source_dir_path=`pwd`
		sec_archive_f=`find $source_dir_path -print | grep -E 'tar.gz|tar.xz|tar.bz2' | grep -v ".diff"`
		path_sec_archive_f=`readlink -f $sec_archive_f`
		count_dir=`ls $dir_path/$year_dir/03/$source_dir | wc -l`
			if [[ $count_dir -lt 3 ]]; then
				echo $path_sec_archive_f >> $dir_path/pkgdiff_$pc_date.files
			else 
				echo $path_archive_f >> $dir_path/pkgdiff_$pc_date.files
			fi
		cd ../

	cd $dir_path/$year_dir/07
	echo `pwd`	
		archive_f=`ls -S | grep ".tar." | grep ".orig" | grep -v ".diff" | head -1`
		path_archive_f=`readlink -f $archive_f`
		source_dir=`ls --file-type -1 | grep "/" | awk -F '/' '{print $1}'`
		cd $dir_path/$year_dir/07/$source_dir
		source_dir_path=`pwd`
		sec_archive_f=`find $source_dir_path -print | grep -E 'tar.gz|tar.xz|tar.bz2' | grep -v ".diff"`
		path_sec_archive_f=`readlink -f $sec_archive_f`
		count_dir=`ls $dir_path/$year_dir/07/$source_dir | wc -l`
			if [[ $count_dir -lt 3 ]]; then
				echo $path_sec_archive_f >> $dir_path/pkgdiff_$pc_date.files
			else 
				echo $path_archive_f >> $dir_path/pkgdiff_$pc_date.files
			fi
		cd ../

	cd $dir_path
done
chmod 444 pkgdiff_$pc_date.files
for i in `seq 1 27`
do
	mkdir report_$i
	j=$(( $i+1 ))
	var_pdif_1=`sed -n "$i"p pkgdiff_$pc_date.files`
	var_pdif_2=`sed -n "$j"p pkgdiff_$pc_date.files`
	if [[ ( -z $var_pdif_1 ) || ( -z $var_pdif_2 ) ]]; then
		cd report_$i
		echo "One or both pkgdiff variable are empty" > sources_files_comp.log
	else 
		cd report_$i
		echo $var_pdif_1 >> sources_files_comp.log
		echo $var_pdif_2 >> sources_files_comp.log
		pkgdiff $var_pdif_1 $var_pdif_2
	fi
	cd $dir_path
done

