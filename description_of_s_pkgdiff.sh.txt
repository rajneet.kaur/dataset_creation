#!/bin/bash
echo "This script MUST BE runned after s_sloccount.sh script!"
read -p "Enter package name: " name
echo "Package name is $name"
read -p "Enter path for package source files (ex: /home/jon/$name): " dir_path

year_s=2005
year_e=2018
---------------------------------------
In first section of script we declare wich language we use (bash), we enter path of package that will be analize with slocount (variable dir_path) and we declare variables for start year (year_s) and end year (year_e) that will be parsed in package directory. We change working directory in path dir_path.
---------------------------------------


pc_date=`date '+%Y%m%d%H%M'`
cd $dir_path
touch -f "$dir_path/pkgdiff_$pc_date.files"
---------------------------------------
In the above section we set a new variabe pc_date that will be used for creation of new file named pkgdiff_$pc_date.files.
---------------------------------------


for year_dir in `seq $year_s $year_e`
do
	cd $dir_path/$year_dir/03
	echo `pwd`	
---------------------------------------
We start loop for years (03 and 07 - i describe only for 03, for 07 is the same) directory parsing and we change directory in every year to 03 directory (month directory). In evrey year will have 2 subdirectory (03 and 07). With line    echo `pwd`    we will print our path (just for control). The loop itself has two similar sections for month 03 and for month 07.
---------------------------------------



	archive_f=`ls -S | grep ".tar." | grep ".orig" | grep -v ".diff" | head -1`
		path_archive_f=`readlink -f $archive_f`
---------------------------------------		
At this section to archive_f variable will take the value from "ls -S" (-S param is for sort descending, largest first) output, and we will do grep on this output after 3 diferent strings (".tar.", ".orig", ".diff") and after that we print only first line (head -1)
With    echo $archive_f   we print on screen value of archive_f variable.		
---------------------------------------



        source_dir=`ls --file-type -1 | grep "/" | awk -F '/' '{print $1}'`
		cd $dir_path/$year_dir/03/$source_dir
		source_dir_path=`pwd`
		sec_archive_f=`find $source_dir_path -print | grep -E 'tar.gz|tar.xz|tar.bz2' | grep -v ".diff"`
		path_sec_archive_f=`readlink -f $sec_archive_f`
		count_dir=`ls $dir_path/$year_dir/03/$source_dir | wc -l`
			if [[ $count_dir -lt 3 ]]; then
				echo $path_sec_archive_f >> $dir_path/pkgdiff_$pc_date.files
			else 
				echo $path_archive_f >> $dir_path/pkgdiff_$pc_date.files
			fi
		cd ../
--------------------------------------
Here is section after typs of archives and which one contain source files will be identified we will check how many directorys contain path of our package:
        count_dir=`ls $dir_path/$year_dir/03/$source_dir | wc -l`
After that if count of dir is lower than 3, the path stored in variable $path_sec_archive_f will be written in pkgdiff_$pc_date.files (that was declared in the begining) and if count of dir will be greater than 3, the path stored in variable $path_archive_f will be written in pkgdiff_$pc_date.files.
            if [[ $count_dir -lt 3 ]]; then
				echo $path_sec_archive_f >> $dir_path/pkgdiff_$pc_date.files
			else 
				echo $path_archive_f >> $dir_path/pkgdiff_$pc_date.files
			fi
--------------------------------------		
		

		
		
	cd $dir_path
done
chmod 444 pkgdiff_$pc_date.files
for i in `seq 1 27`
do
	mkdir report_$i
	j=$(( $i+1 ))
	var_pdif_1=`sed -n "$i"p pkgdiff_$pc_date.files`
	var_pdif_2=`sed -n "$j"p pkgdiff_$pc_date.files`
	if [[ ( -z $var_pdif_1 ) || ( -z $var_pdif_2 ) ]]; then
		cd report_$i
		echo "One or both pkgdiff variable are empty" > sources_files_comp.log
	else 
		cd report_$i
		echo $var_pdif_1 >> sources_files_comp.log
		echo $var_pdif_2 >> sources_files_comp.log
		pkgdiff $var_pdif_1 $var_pdif_2
	fi
	cd $dir_path
done
--------------------------------------
In above section after we change rights on pkgdiff_$pc_date.files only for reading we create a new loop where i variable can be in range 1 to 27 (this mean (2018-2005)x2months) and we create 2 new variables var_pdif_1 and var_pdif_2 (pkgdiff need 2 variables to compare). In this 2 variables will be stored paths from 
pkgdiff_$pc_date.files. 
	var_pdif_1=`sed -n "$i"p pkgdiff_$pc_date.files`
	var_pdif_2=`sed -n "$j"p pkgdiff_$pc_date.files`
If variables are empty (for some years can be empty) output will be "One or both pkgdiff variable are empty" and will be written in sources_files_comp.log
If variables contain paths of sources files, this paths will be written in sources_files_comp.log and after that we run pkgdiff with those 2 paths as variables of pkgdiff application.
