#!/bin/bash
read -p "Enter package name: " name
echo "Package name is $name"
read -p "Enter path for package source files (ex: /home/jon/$name): " dir_path
read -p "Enter start year: " year_s
read -p "Enter end year: " year_e

cd $dir_path

for year_dir in `seq $year_s $year_e`
do
	cd $dir_path/$year_dir/03
	echo `pwd`	
		archive_f=`ls -S | grep ".tar." | grep ".orig" | grep -v ".diff" | head -1`
		echo $archive_f
		if [[ $archive_f =~ ".gz" ]]; then 
			echo "File with GZ extension"
			tar xvzf $archive_f
			source_dir=`ls --file-type -1 | grep "/" | awk -F '/' '{print $1}'`
			cd $dir_path/$year_dir/03/$source_dir
			source_dir_path=`pwd`
			sec_archive_f=`find $source_dir_path -print | grep -E 'tar.gz|tar.xz|tar.bz2' | grep -v ".diff"`
			if [[ $sec_archive_f =~ ".gz" ]]; then
				tar xvzf $sec_archive_f
			elif [[ $sec_archive_f =~ ".xz" ]]; then 
				tar xvf $sec_archive_f
			else
				tar xvjf $sec_archive_f
			fi
			cd ../
			sloccount $source_dir_path > sloccount\_$year_dir\_03.log
		elif [[ $archive_f =~ ".xz" ]]; then 
			echo "File with XZ extension"
			tar xvf $archive_f
			source_dir=`ls --file-type -1 | grep "/" | awk -F '/' '{print $1}'`
			cd $dir_path/$year_dir/03/$source_dir
			source_dir_path=`pwd`
			sec_archive_f=`find $source_dir_path -print | grep -E 'tar.gz|tar.xz|tar.bz2' | grep -v ".diff"`
			if [[ $sec_archive_f =~ ".gz" ]]; then
				tar xvzf $sec_archive_f
			elif [[ $sec_archive_f =~ ".xz" ]]; then 
				tar xvf $sec_archive_f
			else
				tar xvjf $sec_archive_f
			fi
			cd ../
			sloccount $source_dir_path > sloccount\_$year_dir\_03.log
		elif [[ $archive_f =~ ".bz2" ]]; then 
			echo "File with bz2 extension"
			tar xvjf $archive_f
			source_dir=`ls --file-type -1 | grep "/" | awk -F '/' '{print $1}'`
			cd $dir_path/$year_dir/03/$source_dir
			source_dir_path=`pwd`
			sec_archive_f=`find $source_dir_path -print | grep -E 'tar.gz|tar.xz|tar.bz2' | grep -v ".diff"`
			if [[ $sec_archive_f =~ ".gz" ]]; then
				tar xvzf $sec_archive_f
			elif [[ $sec_archive_f =~ ".xz" ]]; then 
				tar xvf $sec_archive_f
			else
				tar xvjf $sec_archive_f
			fi
			cd ../
			sloccount $source_dir_path > sloccount\_$year_dir\_03.log
		fi
	cd $dir_path/$year_dir/07
	echo `pwd`	
		archive_f=`ls -S | grep ".tar." | grep ".orig" | grep -v ".diff" | head -1`
		echo $archive_f
		if [[ $archive_f =~ ".gz" ]]; then 
			echo "File with GZ extension"
			tar xvzf $archive_f
			source_dir=`ls --file-type -1 | grep "/" | awk -F '/' '{print $1}'`
			cd $dir_path/$year_dir/07/$source_dir
			source_dir_path=`pwd`
			sec_archive_f=`find $source_dir_path -print | grep -E 'tar.gz|tar.xz|tar.bz2' | grep -v ".diff"`
			if [[ $sec_archive_f =~ ".gz" ]]; then
				tar xvzf $sec_archive_f
			elif [[ $sec_archive_f =~ ".xz" ]]; then 
				tar xvf $sec_archive_f
			else
				tar xvjf $sec_archive_f
			fi
			cd ../
			sloccount $source_dir_path > sloccount\_$year_dir\_07.log
		elif [[ $archive_f =~ ".xz" ]]; then 
			echo "File with XZ extension"
			tar xvf $archive_f
			source_dir=`ls --file-type -1 | grep "/" | awk -F '/' '{print $1}'`
			cd $dir_path/$year_dir/07/$source_dir
			source_dir_path=`pwd`
			sec_archive_f=`find $source_dir_path -print | grep -E 'tar.gz|tar.xz|tar.bz2' | grep -v ".diff"`
			if [[ $sec_archive_f =~ ".gz" ]]; then
				tar xvzf $sec_archive_f
			elif [[ $sec_archive_f =~ ".xz" ]]; then 
				tar xvf $sec_archive_f
			else
				tar xvjf $sec_archive_f
			fi
			cd ../
			sloccount $source_dir_path > sloccount\_$year_dir\_07.log
		elif [[ $archive_f =~ ".bz2" ]]; then 
			echo "File with bz2 extension"
			tar xvjf $archive_f
			source_dir=`ls --file-type -1 | grep "/" | awk -F '/' '{print $1}'`
			cd $dir_path/$year_dir/07/$source_dir
			source_dir_path=`pwd`
			sec_archive_f=`find $source_dir_path -print | grep -E 'tar.gz|tar.xz|tar.bz2' | grep -v ".diff"`
			if [[ $sec_archive_f =~ ".gz" ]]; then
				tar xvzf $sec_archive_f
			elif [[ $sec_archive_f =~ ".xz" ]]; then 
				tar xvf $sec_archive_f
			else
				tar xvjf $sec_archive_f
			fi
			cd ../
			sloccount $source_dir_path > sloccount\_$year_dir\_07.log
		fi
	cd $dir_path
	echo `pwd`
done

